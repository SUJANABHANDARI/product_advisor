from odoo import api, fields, models
import logging
_logger = logging.getLogger(__name__)


class Site(models.Model):
    _name = 'proadvisor.site'
    _description = 'Ecommerce Site'

    name = fields.Char(string='Name')
    url = fields.Char(string="URL")
    description = fields.Text(string='Description')
    product_ids = fields.One2many(comodel_name='proadvisor.product', inverse_name='site_id', string='Products')


class Subscription(models.Model):
    _name = 'proadvisor.subscription'
    _description = 'New Description'

    # user_id = fields.Many2many('res_users', ondelete='set null', string="Responsible", index=True)
    product_ids = fields.Many2many(comodel_name='proadvisor.product', string='Product Subscription')
    email = fields.Char(string="Email", help="Email")

    # @api.multi
    # def email-subscribe-button(self):
    #     template = unsent_email.env.ref('proadvisor.welcome_template')
    #     self.env['mail.template'].browse(template.id).send_mail(self.id)

