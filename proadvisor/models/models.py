# -*- coding: utf-8 -*-
import logging
from odoo import models, fields, api
_logger = logging.getLogger(__name__)


class Product(models.Model):
    _name = 'proadvisor.product'
    _description = "Product"

    name = fields.Char(string='Name', help="Product Name")
    brand = fields.Char(string='Brand', help="Product Brand")
    model = fields.Char(string='Model', help="Product Model")
    warranty = fields.Integer(string='Warranty', help="Product Warranty")
    description = fields.Text(string='Description', help="Product Description")
    description_html = fields.Html(string='Details', help="Product Description")
    color = fields.Integer()
    product_url = fields.Text(string="Product url")
    image_url = fields.Text(string='Image url')
    uri = fields.Text(string='product url')
    
    site_id = fields.Many2one(comodel_name='proadvisor.site', string='Site', help="Site")
    productcategory_id = fields.Many2one(comodel_name='proadvisor.productcategory', string='Product Caregory')
    price_ids = fields.One2many(comodel_name='proadvisor.price',inverse_name='product_id',string='Prices')
    subscription_ids = fields.Many2many(comodel_name='proadvisor.subscription', string='Subscription')
    daraz_id = fields.Many2one(comodel_name='proadvisor.daraz', string='Products')
    muncha_id = fields.Many2one(comodel_name='proadvisor.muncha', string='Products')

  
class ProductCategory(models.Model):
    _name = 'proadvisor.productcategory'
    _description = "Product Category"

    name = fields.Char(string='Name')
    product_ids = fields.One2many(comodel_name='proadvisor.product', inverse_name='productcategory_id', string='Product')
    

class Price(models.Model):
    _name = 'proadvisor.price'
    _description = 'Product Price'

    price = fields.Float(string='Price')
    currency = fields.Char(string='currency')
    color = fields.Integer()
    product_id = fields.Many2one(comodel_name='proadvisor.product', string='Product')

    @api.model
    def create(self, vals):
        price_id = super(Price, self).create(vals)
        if price_id.product_id:
            if len(price_id.product_id.price_ids) > 1:
                # TODO Only create the email manager object if the price has actually drop
                product_prices = self.env['proadvisor.price'].sudo().search([('product_id', '=', price_id.product_id.id)], order="create_date desc", limit=2)
                if product_prices[0].price < product_prices[1].price:
                    emailmanager_id = self.env['proadvisor.emailmanager'].sudo().create({'product_id': price_id.product_id.id})
        return price_id
