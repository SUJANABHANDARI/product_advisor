from odoo import models, fields, api
import logging
from bs4 import BeautifulSoup
from urllib.request import Request, urlopen
_logger = logging.getLogger(__name__)
import random

class MScrapper(models.Model):
    _name = 'proadvisor.muncha'
    _description = 'Product Scrapper'

    product_ids= fields.One2many(comodel_name='proadvisor.product', inverse_name='muncha_id', string='Munhca product')
    def get_parent_class(self, soup):
        classes = soup.find_all('section', class_="products")[0].find_all('div', recursive=False)[0].get('class')
        _logger.warning(classes)
        return " ".join(classes)
    @api.multi
    def mscrap(self):
        search_categories = self.env['proadvisor.searchcategory'].sudo().search([])
        if search_categories:
            search_category = random.choice(search_categories)    
            get_url_muncha = Request("http://www.shop.muncha.com/Search.aspx?MID=1&q="+search_category.name, headers={'User-Agent': 'Mozilla/5.0'})
            url_open_muncha = urlopen(get_url_muncha)
            url_read_muncha = url_open_muncha.read()
            url_open_muncha.close()
            url_open_muncha_final = url_open_muncha.geturl()
            get_url_muncha = Request(url_open_muncha_final, headers={'User-Agent': 'Mozilla/5.0'})
            url_open_muncha = urlopen(get_url_muncha)
            url_read_muncha = url_open_muncha.read()
            url_open_muncha.close()

            url_soup = BeautifulSoup(url_read_muncha, "html.parser")
            muncha_products = url_soup.find_all('div', {"class":"panel panel-default"})
            for product in muncha_products:
                product_link_all= product.find("div", {"class":"col-md-3 col-sm-4"})
                final_product_a_tag =  product.find("div",{"class":"col-md-9 col-sm-8"})
                product_link = product_link_all.a.get('href')
                final_product_img_link= product_link_all.find('img')['src']
                get_url_muncha2 = Request(product_link, headers={'User-Agent': 'Mozilla/5.0'})
                url_open_muncha2 = urlopen(get_url_muncha2)
                url_read_muncha2 = url_open_muncha2.read()
                url_open_muncha.close()
                url_soup2 = BeautifulSoup(url_read_muncha2, "html.parser")
                final_product_title_muncha = url_soup2.find("h4", {"class":"mobile-modify"}).text.strip()
                product_price = url_soup2.find("h3", {"class":"dtl-price"})
                final_price_muncha = product_price.find("span", {"id":"ItemsBody_lblPrices"}).find(text=True, recursive=False)
                final_price_muncha = final_price_muncha.strip()[3:].replace(",", "")
                display_features_muncha = url_soup2.find('div', {"class":"tab-content"})
                try:
                    if not display_features_muncha.find('ul'):
                        description = " "
                        _logger.warning("*************")
                        _logger.warning("failed")
                        _logger.warning("*************")
                    else:
                        description_list = display_features_muncha.ul.find_all('li')
                        _logger.warning("*************")
                        description = ""

                        for desc in description_list:
                            product_desc = desc.text.strip()
                except:

                    _logger.warning("Exception Error")
                product_exist_muncha = self.env['proadvisor.product'].sudo().search([('name', '=',final_product_title_muncha)], limit = 1)
                _logger.warning(str(final_product_title_muncha))
                try:
                    if product_exist_muncha:
                        _logger.warning("hello this is if part")
                        self.env['proadvisor.price'].sudo().create({'price':final_price_muncha,'product_id': product_exist_muncha.id})

                        _logger.warning(str(final_price_muncha))
                    else:
                        _logger.warning("hello this is else part")
                        new_product_muncha = self.env['proadvisor.product'].sudo().create({'name':final_product_title_muncha,"image_url": final_product_img_link,"product_url":product_link,'description':product_desc})
                        self.env['proadvisor.price'].sudo().create({'price':final_price_muncha, 'product_id':new_product_muncha.id})
                        _logger.warning(str(final_price_muncha))
                except:
                    _logger.warning("Exception")
                    return False 