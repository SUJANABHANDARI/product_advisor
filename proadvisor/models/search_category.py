from odoo import api, fields, models


class SearchCategory(models.Model):
    _name = 'proadvisor.searchcategory'
    _description = 'Search Category'

    name = fields.Char(string='Name')
