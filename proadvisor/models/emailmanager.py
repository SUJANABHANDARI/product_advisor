from odoo import api, fields, models
import logging
_logger = logging.getLogger(__name__)


class EmailManager(models.Model):
    _name = 'proadvisor.emailmanager'
    _description = 'Email Manager'

    name = fields.Char(string='Name')
    product_id = fields.Many2one(comodel_name='proadvisor.product', string='Product', required=True)
    is_sent = fields.Boolean(string="Is Sent")
    email = fields.Text(string="Email")

    @api.model
    def create(self, vals):
        emailManager_id = super(EmailManager, self).create(vals)
        if emailManager_id:
            subscribers = self.env['proadvisor.subscription'].sudo().search([('product_ids', '=', emailManager_id.product_id.id)])
            if subscribers:
                email_list = ""
                for subscriber in subscribers:
                    email_list = email_list + subscriber.email + ","
                email_list = email_list[:-1]
                emailManager_id.email = email_list
            else:
                emailManager_id.email = ""
            emailManager_id.write({
                'name': emailManager_id.product_id.name + " - Email manager"
            })
        return emailManager_id
    
    @api.multi
    def write(self, vals):
        success = super(EmailManager, self).write(vals)
        if success:
            if 'product_id' in vals:
                subscribers = self.env['proadvisor.subscription'].sudo().search([('product_ids', '=', self.product_id.id)])
                if subscribers:
                    email_list = ""
                    for subscriber in subscribers:
                        email_list = email_list + subscriber.email + ","
                    email_list = email_list[:-1]
                    self.email = email_list
                else:
                    self.email = ""
                self.write({
                    'name': self.product_id.name + " - Email manager"
                })
        return success


    @api.model
    def send_mail(self):
        unsent_emails = self.env['proadvisor.emailmanager'].sudo().search([('is_sent', '=', False)])
        for unsent_email in unsent_emails:
            template = unsent_email.env.ref('proadvisor.email_template')
            unsent_email.env['mail.template'].browse(template.id).send_mail(unsent_email.id)
            unsent_email.is_sent = True
