# -*- coding: utf-8 -*-
from odoo import models, fields, api
import logging
import random
from bs4 import BeautifulSoup
from urllib.request import Request, urlopen
_logger = logging.getLogger(__name__)


class Scrapper(models.Model):
    _name = 'proadvisor.daraz'
    _description = 'Product Scrapper'

    product_ids= fields.One2many(comodel_name='proadvisor.product', inverse_name='daraz_id', string='Daraz product_darazs')

    # Scrap parent class for better scrapping
    # @return string
    def get_parent_class(self, soup):
        classes = soup.find_all('section', class_="products")[0].find_all('div', recursive=False)[0].get('class')
        _logger.warning(classes)
        return " ".join(classes)
    
    def dscrap(self):
        search_categories = self.env['proadvisor.searchcategory'].sudo().search([])
        if search_categories:
            search_category = random.choice(search_categories)
            _logger.warning(str(search_category.name))
            get_url =Request("https://www.daraz.com.np/catalog/?q="+search_category.name, headers={'User-Agent': 'Mozilla/5.0'})
            url_open = urlopen(get_url)
            url_open.close()

            final_url_daraz = url_open.geturl()
            get_url = Request(final_url_daraz, headers={'User-Agent': 'Mozilla/5.0'})
            url_open = urlopen(get_url)
            url_read = url_open.read()
            url_open.close()

            url_soup = BeautifulSoup(url_read, "html.parser")
            
            # Find dynamic class for the product item scrap
            scrap_class = self.get_parent_class(url_soup)
            
            products = url_soup.find_all('div', class_=scrap_class)
            # products = random.sample(products, 5)
            _logger.warning(str(len(products)))
            for product in products:
                brand_name = product.a.h2.find_all("span", {"class":"brand"})
                title = product.a.h2.find_all("span", {"class":"name"})
                name = title[0].text.strip()
                price_sale = product.find("div", {"price-container clearfix"})
                price = product.find("span", {"class":"price"}).span.find_next_sibling("span").get("data-price")
                product_url = product.a.get('href')
                product_image = product.find("div",{"class":"image-wrapper default-state"})
                product_img = product_image.find('img')
                product_image_url = product_img.get('data-src')
                get_url2 =Request(product_url, headers={'User-Agent': 'Mozilla/5.0'})
                url_open2 = urlopen(get_url2)
                url_read2 = url_open2.read()
                url_open2.close()
                url_soup2 = BeautifulSoup(url_read2, "html.parser")
                product_detail = url_soup2.find('section', class_="sku-detail")
                product_wrapper = product_detail.find('div',class_="details-wrapper")
                product_detail_description = product_wrapper.find('div',class_="detail-features")
                display_features = product_detail_description.find('div',class_="list -features -compact -no-float")

                try:
                    if display_features:
                        if not display_features.find('ul'):
                            description_list = "<ul/>"
                        else:
                            description_list = display_features.ul
                            _logger.warning(description_list)
                    else:
                        description_list = "<ul/>"
                except:
                    _logger.warning("Description not found")

                product_exist = self.env['proadvisor.product'].sudo().search([('name', '=', name)], limit = 1)
                try:
                    if product_exist:
                        self.env['proadvisor.price'].sudo().create({'price':price,"product_id": product_exist.id})
                    else:
                        new_product = self.env['proadvisor.product'].sudo().create({'name': name,"image_url":product_image_url,"description_html":description_list,'product_url':product_url})
                        self.env['proadvisor.price'].sudo().create({'price':price,'product_id':new_product.id})
                except:
                    _logger.warning("Exception")
        else:
            _logger.warning("This is a false part")
            return False