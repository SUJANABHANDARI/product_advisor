# -*- coding: utf-8 -*-
{
    'name': "proadvisor",

    'summary': """
        Find the ideal product and compare prices from different websites""",

    'description': """
        Long description of module's purpose
    """,

    'author': "Sujana Bhandari",
    'website': "http://www.s2k.com",

    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/master/odoo/addons/base/module/module_data.xml
    # for the full list
    'category': 'Ecommerce',
    'version': '0.1',

    # any module necessary for this one to work correctly
    'depends': ['website', 'mail','auth_oauth'],

    # always loaded
    'data': [
        # 'security/security.xml',
        'security/ir.model.access.csv',
        'data/darazscrapper_cron.xml',
        'data/munchascrapper_cron.xml',
        'data/cron_data.xml',
        'views/assets.xml',
        'views/layout.xml',
        'views/footerheader.xml',
        'views/template.xml',
        'views/views.xml',
        'views/login.xml',
        'views/signup.xml',
        'views/aboutus.xml',
        'views/forgetpassword.xml',
        'views/emailtemplate.xml',
        'views/email_manager_views.xml',
        'views/search_category_view.xml',
        'views/scrapper_views.xml', 
        'views/login_signup_extend.xml',

    ],
    # only loaded in demonstration mode
    'demo': [
        'demo/demo.xml',
    ],
    # 'application': True,
}