# # -*- coding: utf-8 -*-
from odoo import http
import logging
from odoo.http import request, route
import json
import re
from odoo.addons.website.controllers.main import Website
_logger = logging.getLogger(__name__)

from odoo.addons.http_routing.models.ir_http import slug


class Proadvisor(http.Controller):
    @http.route(['/proadvisor/home/','/proadvisor/home'], auth='public', website=True)
    def index(self, **kw):
        Products = http.request.env['proadvisor.product']
        return http.request.render('proadvisor.index', {
            'products': Products.search([])
        })

    @http.route(['/products/<model("proadvisor.product"):product>','/products/<model("proadvisor.product"):product>/'], auth='public', website=True)#<angle bracket=variable>
    def teacher(self, product):
        return http.request.render('proadvisor.product_description', {
            'item': product
        })

    

    @http.route('/proadvisor/login', auth="public", website=True)
    def loginform(self, **kw):
        return http.request.render('proadvisor.login_template')

    @http.route('/aboutus', auth="public", website=True)
    def aboutus(self, **kw):
        return http.request.render('proadvisor.proadvisor_aboutus')

    @http.route('/contact', auth="public", website=True)
    def contact(self, **kw):
        return http.request.render('proadvisor.proadvisor_contact')

    @http.route('/proadvisor/subscribe', auth="public", website=True)
    def subscribebutton(self, **kw):
        
        if request.httprequest.method =="POST":
            email = request.params.get("email")
            product_id = int(request.params.get("product_id"))
            
            try:
                email_exist = request.env['proadvisor.subscription'].sudo().search([('email', '=', email)], limit = 1)
                if email_exist:
                    email_exist.sudo().write({"product_ids":[(4, product_id, 0)]})
                    message = "You have already successfully subscribed this product."
                else:
                    user_subscribe = request.env['proadvisor.subscription'].sudo().create({'email':email,'product_ids':[(4, product_id, 0)]})
                    message = "You have successfully subscribed this product."
                return json.dumps({'status':'success', 'message':message})
            except Exception as e:
                return json.dumps({'status':'failure', 'message':str(e)})

    @http.route('/proadvisor/signup', auth="public", website=True)
    def signup(self, **kw):        
        if request.httprequest.method  == 'POST':
            first_name = request.params.get("first_name")
            last_name = request.params.get("last_name")
            email = request.params.get("email")
            password = request.params.get("password")
            confirm_password = request.params.get("confirm_password")
            _logger.warning("helloo sujana")


            user = request.env['res.users'].sudo().search([('login', '=', email)]) 
            if user:
                _logger.warning("inside user login")
                return json.dumps({'status':'failed', 'message':'User with %s already exists.' % email}) 
            else:
            
                _logger.warning("inside else part of user login")

                partner = request.env['res.partner'].sudo().create({'name':first_name, 'email':email})

                if partner:
                    _logger.warning("inside else part of user login")
                    user = request.env['res.users'].sudo().create({'active':'true', 'login': email, 'partner_id': partner.id, 'password': password})
                    return json.dumps({'status':'success', 'user_id':user.id})
                else:
                    return json.dumps({'status':'failed', 'message':'Could not create user'})
                # partner_user = partner.user_ids and partner.user_ids[0] or False
            #     return request.env['res.users'].sudo().create({
            #         'first_name' = first_name, 
            #         'last_name' = last_name, 
            #         'email' = email, 
            #         'password' = password,
            #         'confirm_password' = confirm_password

        # _logger.warning("hello %s %s with %s and %s" % (first_name,last_name,email,password,confirm_password))
            # return json.dumps({'status':'success'})
        else :
            return http.request.render('proadvisor.signup_template')



    
    @http.route('/proadvisor/resetpassword', auth="public", website=True)
    def resetpassword(self, **kw):
        return http.request.render('proadvisor.forget_password')
    
    @route(['/proadvisor/products'], type='json', auth='public', website=True)
    def get_products(self, id=None, fields=None, **kw):
        if id:
            domain = [("id", "=", id)]
        else:
            domain = None
        products = request.env['proadvisor.product'].sudo().search_read(domain, fields)
        
        ids = []
        for product in products:
            ids.append(product['id'])
        
        product_ids = request.env['proadvisor.product'].sudo().browse(ids)
        for x in range(len(products)):
            products[x]['slug'] = "/products/%s" % slug(product_ids[x])
            _logger.warning(products[x]['slug'])
        
        return products

    
    @route(['/search/<string:name>', '/search/'], type='json', auth='public', website=True)
    def search_product(self, name="", **kw):
        if request.httprequest.method =="POST":
            if name != "":
                domain = [('name', 'ilike', name )]
            else:
                domain = []
            
            products = request.env['proadvisor.product'].sudo().search_read(domain)
            
            # _logger.warning(products);
            ids = []
            for product in products:
                ids.append(product['id'])
            
            product_ids = request.env['proadvisor.product'].sudo().browse(ids)
            for x in range(len(products)):
                products[x]['slug'] = "/products/%s" % slug(product_ids[x])
            
            return products


class ProadvisorHome(Website):
    @http.route('/', type='http', auth="public", website=True)
    def index(self, **kw):
        homepage = request.website.homepage_id
        if homepage and (
                homepage.sudo().is_visible or request.env.user.has_group('base.group_user')) and homepage.url == '/':
            Products = http.request.env['proadvisor.product']
            return http.request.render('proadvisor.index', {
                'products': Products.search([])
            })

        website_page = request.env['ir.http']._serve_page()
        if website_page:
            return website_page
        else:
            top_menu = request.website.menu_id
            first_menu = top_menu and top_menu.child_id and top_menu.child_id.filtered(lambda menu: menu.is_visible)
            if first_menu and first_menu[0].url not in ('/', '') and (
                    not (first_menu[0].url.startswith(('/?', '/#', ' ')))):
                Products = http.request.env['proadvisor.product']
                return http.request.render('proadvisor.index', {
                    'products': Products.search([])
                })

        raise request.not_found()