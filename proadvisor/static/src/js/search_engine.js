odoo.define('proadvisor.SearchEngine',function (require)
{
    "use strict";

    var Widget = require('web.Widget');
    var core = require('web.core');

   /* for search field*/
    var SearchEngine = Widget.extend
    ({
    
        template: 'SearchForm',
        xmlDependencies: ['/proadvisor/static/src/xml/proadvisor.xml'],

        init: function (parent){
            this._super(parent);
            console.log("widget started");
        },
        start: function () {
            this._super(parent);
            console.log("widget started");
        },
    });

    var searchengine = SearchEngine(this);
    searchengine.appendTo(".search-form");

});