odoo.define('proadvisor.views', function(require) {
    'use strict';

    var bus = require('bus.bus').bus;
    var core = require('web.core');
    var Dialog = require('web.Dialog');
    var ProductsSearchlist = require('proadvisor.classes').ProductsSearchlist;
    var Widget = require('web.Widget');
    var qweb = core.qweb;
    var _t = core._t;
    var ajax = require('web.ajax');
    require('web.dom_ready');

    var ProadvisorApp = Widget.extend({
        template: 'proadvisor.app',
        xmlDependencies: ['/proadvisor/static/src/xml/product_views.xml'],
        /* Lifecycle */
        init: function(parent, options) {
            console.log("hello this is a controller part")
            this._super.apply(this, arguments);
            this.productsearchlist = new ProductsSearchlist({ id: odoo.csrf_token });
        },
        willStart: function() {
            return $.when(this._super.apply(this, arguments),
                this.productsearchlist.fetchAllProducts()
                // this.productsearchlist.search()
            ).then(function(productsearchlist) {
                bus.update_option('proadvisor.product', '1');
            });
        },

        start: function() {
            var self = this;
            return this._super.apply(this, arguments).then(function() {
                // Create and append branch list
                self.list = new ProductList(self, self.productsearchlist.products);
                self.list.appendTo($('.o_product_list'));


                // Create and append search widget
                self.search = new ProductSearch(self, self.productsearchlist.products);
                self.search.appendTo($('.o_product_search'));

                core.bus.on('onProductSearch', self, self._showProducts);
            });


            // self.notification_manager = new notification.NotificationManager(self);
            // self.notification_manager.appendTo(self.$el);


            // core.bus.on('onClickPrevBranch', self, self._prevBranch);

            // Register events in bus
            // bus.on('notification', self, self._onNotification);
        },
        _showProducts: function(products) {
            var self = this;
            self.list.products = products;
            var product_list = qweb.render('proadvisor.product_list', { widget: self.list });
            this.$('.product_list').replaceWith(product_list);
        },
    });

    var ProductList = Widget.extend({
        template: 'proadvisor.product_list',
        /* Lifecycle */
        init: function(parent, products) {
            this._super.apply(this, arguments);
            this.products = products;
        },
        /**
         * Insert a new product instance in the list. If the list is hidden
         * (because there was no product prior to the insertion), call for
         * a complete rerendering instead.
         * @param  {OdooClass.product} branch Branch to insert in the list
         */
        insertProduct: function(product) {
            // if (!this.$('.modal-body').length) {
            //     this._rerender();
            //     return;
            var product_node = qweb.render('proadvisor.product_list.product', { product: product });
            this.$('.product_list').appendTo(product_node);
        },
        /**
         * Update an existing branch instance in the list.
         * @param  {OdooClass.Branch} branch Branch to update in the list
         */
        updateProduct: function(poduct) {
            var product_node = qweb.render('proadvisor.product_list.product', { product: product });
            this.$('div[data-id=' + product.id + ']').replaceWith(product_node);
        },
        /**
         * Remove a branch from the list. If this is the last branch to be
         * removed, rerender the widget completely to reflect the 'empty list'
         * state.
         * @param  {Integer} id ID of the branch to remove.
         */
        removeProduct: function(id) {
            this.$('div[data-id=' + id + ']').remove();
            if (!this.$('div[data-id]').length) {
                this._rerender();
            }
        },

        /**
         * Rerender the whole widget; will be useful when we switch from
         * an empty list of branches to one or more branch (or vice-versa)
         * by using the bus.
         */
        _rerender: function() {
            this.replaceElement(qweb.render('proadvisor.product_list', { widget: this }));
        },
    });


    var ProductSearch = Widget.extend({

        template: 'proadvisor.proadvisor_title',
        // events: {'click button.email-subscribe': '_onemailsubscribebutton',
        //         },
        xmlDependencies: ['/proadvisor/static/src/xml/product_views.xml'],


        init: function(parent, product) {
            this.productsearchlist = parent.productsearchlist;
            this._super.apply(this, arguments);
            this.parent = parent;
        },
        willStart: function() {
            return $.when(this._super.apply(this, arguments),
                this.productsearchlist.fetchAllProducts()
                // this.productsearchlist.search()
            ).then(function(productsearchlist) {
                bus.update_option('proadvisor.product', '1');
            });
        },
        start: function() {
            var self = this;
            console.log("This is a start part");
            var self = this;
            self.product_search_input = self.$el.find("#product_search_input");
            self.$el.find(".search-button").on("click", _.bind(self._onsearchbutton, self));

            self.$el.find("#product_search_input").on("keypress", _.bind(self._onkeyEnter, self));
            // self.$el.find("#product_search_input").on("change keydown paste input", _.bind(self._onfieldBlank, self));

            self.$el.find("#product_search_input").on("change paste input", _.bind(self._onfieldBlank, self));
            console.log("Button is detected")
        },

        _onfieldBlank: function(event) {
            var self = this;
            // console.log(event.type);
            // console.log(event.target.value.length);
            if (event.target.value.length == 0) {
                self.parent.productsearchlist.fetchAllProducts(self).then(function(productWidget) {
                    // console.log("hello welcommmmmeeeeeee");
                    var product_list = qweb.render('proadvisor.product_list', { widget: productWidget });
                    self.parent.$('.product_list').replaceWith(product_list);
                });
            }
        },
        _onkeyEnter: function(event) {
            var self = this;

            if (event.keyCode === 13) {
                event.preventDefault();
                console.log("Enter is pressed");
                this._onsearchbutton();
            }
        },
        _onsearchbutton: function() {
            var self = this;
            var form = self.$el;
            console.log("Search button clicked");

            self.productsearchlist.search(self.product_search_input.val().trim()).then(function(products) {
                core.bus.trigger('onProductSearch', products);
            });
        },


    });

    var $elem = $('.o_proadvisor_app');
    var app = new ProadvisorApp(null);
    app.appendTo($elem).then(function() {
        bus.start_polling();
    });
});