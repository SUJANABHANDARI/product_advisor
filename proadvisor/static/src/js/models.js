odoo.define('proadvisor.classes', function(require) {
    'use strict';

    var Class = require('web.Class');
    var rpc = require('web.rpc');

    /**
     * Branches
     * Represents a proadvisor.product object from the Backend
     * @type {OdooClass}
     */
    var Product = Class.extend({
        init: function(values) {
            Object.assign(this, values);
        },
        /**
         * Fetch the latest fields for this particular product
         * on the backend server
         * @return {jQuery.Deferred} Resolves to the updated
         *                product if successful.
         */
        update: function() {
            var self = this;
            return rpc.query({
                model: 'proadvisor.product',
                method: 'read',
                args: [
                    [this.id]
                ],
                kwargs: { fields: ['id', 'name', 'brand', 'model', 'warranty', 'description', 'image_url', 'price_id'] }
            }).then(function(product_values) {
                Object.assign(self, product_values[0]);
                return self;
            });
        },
    });

    /**
     * Map
     * Represents a product search  from the Backend in future, 
     * accessible by default.
    
     * @type {OdooClass}
     */

    var ProductsSearchlist = Class.extend({
        init: function(values) {
            Object.assign(this, values);
            this.products = [];
            this.current_product_index = -1;
        },

        /**
         * Fetch all available products for the user.
         * Note that the actual search is done server side
         * using the model's ACLs and Access Rules.
         * @return {jQuery.Deferred} 
         */
        fetchAllProducts: function() {
            var self = this;
            return rpc.query({
                'route': '/proadvisor/products',
                params: { fields: ['id', 'name', 'brand', 'model', 'warranty', 'description_html', 'product_url', 'image_url', 'price_ids'] },
            }).then(function(product_values) {
                self.products = [];
                for (var vals of product_values) {
                    self.products.push(new Product(vals));
                }
                return self;
            });
        },
        /**
         * Fetch a specified product id for the current productsearch
         * @param  {Integer} id ID of the product to fetch.
         * @return {jQuery.Deferred} Resolves to the new Product
         */
        // fetchProduct: function(id) {
        //     var self = this;
        //     return rpc.query({
        //         'route': '/proadvisor/product/',
        //         params: {
        //             id: id,
        //         },
        //     }).then(function(product_values) {
        //         if (product_values.length) {
        //             var product = new Product(product_values[0]);
        //             var p_idx = self.products.findIndex(p => p.id === id);
        //             if (p_idx !== -1) {
        //                 self.products.splice(p_idx, 1, product);
        //             } else {
        //                 self.products.push(products);
        //             }
        //         }
        //         return branch;
        //     });
        // },
        /**
         * Remove a specified product id from the collections.
         * @param  {Integer} id ID of the product to remove
         */
        search: function(query) {
            var self = this;
            return rpc.query({
                'route': '/search/' + query,
                params: {},
            }).then(function(product_values) {
                var products = [];
                if (product_values.length > 0) {
                    product_values.forEach(product => {
                        products.push(product);
                    });
                }
                return products;
            });
        },
    });

    return {
        Product: Product,
        ProductsSearchlist: ProductsSearchlist,
    };
});