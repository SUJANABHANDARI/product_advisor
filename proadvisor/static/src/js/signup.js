odoo.define('proadvisor.SignUpForm', function(require)
{
    "use strict";
    
    var core = require('web.core');
    var Widget = require('web.Widget');
    var rpc = require('web.rpc');
    var ajax = require('web.ajax');
    var QWeb = core.qweb;
   

    var SignUp = Widget.extend({
        template: 'signuptemplate',
        events: {
            'click button.signup-button': '_onFormSubmitButtonClick',
        },
        xmlDependencies: ['/proadvisor/static/src/xml/signupjs.xml'],

        init: function (parent, value){
            this._super(parent);

         },
        start: function (){
            var self = this;
            $("#success_box").hide();
            $("#warning_box").hide();
        },
        _onFormSubmitButtonClick: function(){
            var sendButton = this.$el.find('.signup-button');
            if (sendButton.hasClass("n")) {
                return false;
            }
            sendButton.off().addClass('disalbled');
            console.log("Button is Clicked");
            var self = this;
            var form = self.$el;
            var first_name = self.$el.find("#first_name");
            var last_name =self.$el.find("#last_name");
            var email =self.$el.find("#email");
            var password =self.$el.find("#password");
            var confirm_password =self.$el.find("#confirm_password");

            console.log("helooo %s" % first_name)

            if (first_name.val().trim() == ""){
                $("#warning_box").find('span').text("Please provide your first name");
                $("#warning_box").show();
                first_name.focus();
                sendButton.off().removeClass('disabled');
               return false;
            }
            if (last_name.val().trim() == ""){
                $("#warning_box").find('span').text("Please provide your last name");
                $("#warning_box").show();
                last_name.focus();
                sendButton.off().removeClass('disabled');
                return false;
            }
            if (email.val().trim() == "") {
                $("#warning_box").find('span').text("Please provide your email ");
                $("#warning_box").show();
                email.focus();
                sendButton.off().removeClass('disabled');
                return false;
            }
            var atpos = email.val().trim().indexOf("@");
            var dotpos = email.val().trim().lastIndexOf(".");
            if (atpos < 1 || ( dotpos - atpos < 2 ))
            {   
                $("#warning_box").find('span').text("Please provide your valid email");
                $("#warning_box").show();
                email.focus();
                sendButton.off().removeClass('disabled');
                return false;
            }
            if (password.val().trim() == "")
            {   
                $("#warning_box").find('span').text("Please provide your password");
                $("#warning_box").show();
                password.focus();
                sendButton.off().removeClass('disabled');
                return false;
            }
            if (confirm_password.val().trim() == "")
            {   
                $("#warning_box").find('span').text("Please provide your confirm password");
                $("#warning_box").show();
                confirm_password.focus();
                sendButton.off().removeClass('disabled');
                return false;
            }
            if(confirm_password.val() != password.val())
                {
                    $("#warning_box").find('span').text("Password is incorrect");
                     $("#warning_box").show();
                }
                else {
                    $("#success_box").find('span').text("Password is correct");
                     $("#success_box").show();

                }
            // _checkMatchingPassword($password,$confirm_password);

            var form_values = {
                "first_name": first_name.val().trim(),
                "last_name": last_name.val().trim(),
                "email": email.val().trim(),
                "password": password.val().trim(),
                "confirm_password": confirm_password.val().trim()
            };

            ajax.post(form.attr('action'), form_values).then( function (result){
                console.log("success");
                sendButton.off().removeClass('disabled');
                result = JSON.parse(result);
                if (result.status == "success")
                {   
                    $("#success_box").find('span').text("Succesfull");
                    $("#success_box").show();
                    first_name.val("");
                    last_name.val("");
                    email.val("");
                    password.val("");
                    confirm_password.val("");
                    console.log("user_id:" + result.user_id);
                }
                else {
                    alert(result.message);
                }
            });
        },
    });
    var signupformvalidation = new SignUp(this);
    signupformvalidation.appendTo('.proadvisor-signup-form');
    return SignUp;
});
// TODO set time out
//how to redirect to new page from a javascript