odoo.define('proadvisor.Subscribe', function(require) {
    "use strict";

    console.log("welcome sujana");

    var core = require('web.core');
    var Widget = require('web.Widget');
    var rpc = require('web.rpc');
    var ajax = require('web.ajax');
    var QWeb = core.qweb;

    var Subscribe = Widget.extend({
        template: 'SubscribeButton',
        // events: {
        //     'click button.button-subscribe': '_onSubscribeButtonClick',
        // },

        xmlDependencies: ['/proadvisor/static/src/xml/subscribejs.xml'],

        init: function(parent, value) {
            this._super(parent);
        },
        start: function() {
            var self = this;
            this._super.apply(this, arguments);
            // console.log(this.$el);
            self.$el.on("click", _.bind(self._onSubscribeButtonClick, self));
            console.log("hello sujana start");
        },
        _onSubscribeButtonClick: function() {
            console.log("Button clicked");

        },
    });


    var subscribeButton = new Subscribe(this);
    subscribeButton.appendTo(".buttonSubscribe");

    var Subscribebutton = Widget.extend({

        template: 'Emailsubscribtion',
        // events: {'click button.email-subscribe': '_onemailsubscribebutton',
        //         },
        xmlDependencies: ['/proadvisor/static/src/xml/subscribejs.xml'],


        init: function(parent, value) {
            this._super.apply(this, arguments);
        },
        start: function() {
            var self = this;
            this._super.apply(this, arguments);
            self.subscribeButton = self.$el.closest("#myModal");
            self.warning = self.$el.closest("div.modal-dialog").find(".warning");
            self.$el.find(".email-subscribe-button").on("click", _.bind(self._onemailsubscribebutton, self));

        },
        _onemailsubscribebutton: function() {
            var self = this;
            // console.log("Hello sujana we have entered function part");
            var submitbutton = this.$el.find('.email-subscribe-button');
            // if (submitbutton.hasClass("disabled"))
            // {
            //     return false;
            // }
            // submitbutton.off().addClass('disabled');
            // console.log("Button clicked44");
            var self = this;
            var form = self.$el;
            var email = self.$el.find("#email");
            var product_id = $('#product_id').attr("data-id");

            if (email.val().trim() == "") {
                self._showDialog("danger", "Email can't be empty.");
                email.focus();
                // submitbutton.off().removeClass("disabled");
                return false;
            }

            var atpos = email.val().trim().indexOf("@");
            var dotpos = email.val().trim().lastIndexOf(".");
            if (atpos < 1 || (dotpos - atpos < 2)) {
                self._showDialog("warning", "Please provide a valid email.");
                email.focus();
                return false;
            }

            var form_values = {
                "email": email.val().trim(),
                "product_id": product_id,
            };

            ajax.post(('/proadvisor/subscribe'), form_values).then(function(result) {
                // console.log(result);
                // // submitbutton.off().removeClass('disabled');
                result = JSON.parse(result);
                // console.log(result);
                if (result.status == "success") {
                    self._showDialog("success", result.message);
                    setTimeout(() => {
                        self.subscribeButton.modal("toggle");
                    }, 3000);
                } else {
                    self._showDialog("danger", result.message);
                }
            });
        },
        _showDialog: function(type, message) {
            var self = this;
            this.warning.attr("class", "alert alert-" + type + " warning");
            this.warning.find('strong').html(this._toUpperCaseFirst(type) + "!");
            this.warning.find('span').html(" <b>" + message + "</b>");
            this.warning.fadeIn("slow");
            setTimeout(() => {
                self.warning.fadeOut("slow");
            }, 3000);
        },
        _toUpperCaseFirst: function(lower) {
            // lower.charAt(0).toUpperCase() + lower.substr(1);
            return lower.replace(/^\w/, c => c.toUpperCase());
        },
    });

    var subscribebutton = new Subscribebutton();
    subscribebutton.appendTo(".emailsubscribebutton");
});