odoo.define('proadvisor.Login',function (require) {
    "use strict";

    var Widget = require('web.Widget');
    var core = require('web.core');

   /* for search field*/
    var Login = Widget.extend ({
        events: {
            'click .login-button': '_onclick',
        },
        template: 'LoginForm',
        xmlDependencies: ['/proadvisor/static/src/xml/proadvisor.xml'],
        init: function (parent) {
            this._super(parent);
            console.log("widget started");
        },
        start: function () {
            var self = this;
            this._super(parent);
            self.email = self.$el.find("#email");
            console.log(self.$el);
            self.password= self.$el.find("#password");
            console.log("widget started");

        },
        _onclick: function () {
            var self = this;
            console.log(self.email.val());
            if (self.email.val() != "") {
                console.log(self.email + " logged in successfully!");
            }
        },
    });

    var login = new Login(this);
    login.appendTo(".login-form");
});